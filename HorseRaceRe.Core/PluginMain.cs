﻿using System;
using System.Linq;
using HorseRaceRe.UI;
using XQColorful.Core.Export;
using XQColorful.SDK.Interface;
using XQColorful.SDK.Models;
using App = HorseRaceRe.Code.App;

namespace XQColorful.Core
{
    public class PluginMain
    {
        public static AppInfo Info() => new AppInfo()
        {
            name = "HorseRaceRe",
            author = "Heer",
            desc = "升级版的赛马",
            pver = "0.2.1"
        };

        public static void RegEvent()
        {
            App.UseCommonService();
            //new Class1(); 请在这里随意使用Code的一个代码以保证add ref的Code程序集能够被正确加载
            //new Class2(); UI项目同理

            var ass = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var item in ass)
            {
                var types = item.GetTypes().ToList().Where(s => s != typeof(IPluginEvent) && typeof(IPluginEvent).IsAssignableFrom(s));
                types.ToList().ForEach(
                    t => _Event.Events.Add((IPluginEvent)Activator.CreateInstance(t)));
            }
        }
    }
}