﻿namespace HorseRaceRe.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public static MainWindow Instance;
        public static void CreateShow()
        {
            Instance = new MainWindow();
            Instance.Show();
            
        }
        public string Logs = "";
        
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;
        }
    }
}