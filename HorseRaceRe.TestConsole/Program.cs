﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using csscript;
using CSScriptLibrary;
using HorseRaceRe.Code;
using HorseRaceRe.Code.Models;
using Mono.CSharp;
using Newtonsoft.Json;

namespace HorseRaceRe.TestConsole
{
    internal class Program
    {
        public static async Task Main(string[] args)
        {
            App.LoadedEvents = App.LoadEvents();
            App.LoadedEvents.ForEach(eventInfo => { Console.WriteLine(eventInfo.Info.Name); });
            Console.ReadKey();
        }
    }


    public interface ITest
    {
        void Process(ref string a);
    }

    public class TestInfo
    {
        public ITest Test;

        public FileInfo Info;
    }
}