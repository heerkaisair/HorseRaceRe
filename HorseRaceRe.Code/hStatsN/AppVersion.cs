﻿using System;

namespace HorseRaceRe.Code.hStatsN
{
    public class AppVersion
    {
        /// <summary>
        /// APPID
        /// </summary>
        public string App { get; set; }

        /// <summary>
        /// 版本
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// 更新日志
        /// </summary>
        public string UpdateLog { get; set; }

        /// <summary>
        /// 下载链接
        /// </summary>
        public string DownloadLink { get; set; }
        
        /// <summary>
        /// 更新时间(默认为NOW())
        /// </summary>
        public DateTime UpdateTime { get; set; }
    }
}