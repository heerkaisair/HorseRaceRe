﻿using System;
using System.Threading.Tasks;
using HorseRaceRe.Code.Helpers;
using Newtonsoft.Json;

namespace HorseRaceRe.Code.hStatsN
{
    public class hStatsN
    {
        private const string Server = "http://www.heerdev.top:4999";

        private string uuid;
        public hStatsN(string appid, string version)
        {
            Appid = appid;
            this.Version = version;
            uuid = Guid.NewGuid().ToString().Replace("-", "");
            Launch();
        }

        public string Appid { get; private set; }
        public string Version { get; private set; }

        /// <summary>
        /// 检查是否需要更新
        /// </summary>
        /// <returns></returns>
        public bool CheckUpdate(out AppVersion version)
        {
            try
            {
                var url = $"{Server}/AppVersions/Latest?appid={Appid}";
                var v = JsonConvert.DeserializeObject<AppVersion>(HttpHelper.Get(url));
                version = v;
                if (v.Version != this.Version)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                version = null;
                return false;
            }
        }

        
        public bool CheckUpdate()
        {
            return CheckUpdate(out var version);
        }

        public void SetOnline()
        {
            try
            {
                HttpHelper.Get($"{Server}/Online/Set/?appid={this.Appid}&uuid={uuid}");
            }
            catch (Exception)
            {
            }
        }
        private void Launch()
        {
            try
            {
                HttpHelper.Get($"{Server}/AppLaunchTimes/Add/?appid={this.Appid}");
                Task.Factory.StartNew(async () =>
                {
                    while (true)
                    {
                        SetOnline();
                        await Task.Delay(TimeSpan.FromHours(0.5));
                    }
                    
                });
            }
            catch (Exception exception)
            {
                System.Console.WriteLine(exception);
                throw;
            }
        }
    }
}