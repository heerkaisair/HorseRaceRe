﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CSScriptLibrary;
using HorseRaceRe.Code.Interfaces;
using HorseRaceRe.Code.Models;
using XQColorful.SDK.Models;

namespace HorseRaceRe.Code
{


    public static class App
    {
        public static hStatsN.hStatsN Core = new hStatsN.hStatsN("top.heerdev.HorseRaceRe","0.2.2-beta1");
        public static List<Task> TaskList =new List<Task>();
        public static Dictionary<string, RaceGround> GroupGrounds = new Dictionary<string, RaceGround>();
        public static bool UseCommonService() => true; //反应引用的状况

        public static AppSets AppSet;

        public static List<BuffInfo> LoadedBuffs = new List<BuffInfo>();
        public static List<EventInfo> LoadedEvents = new List<EventInfo>();

        public static string AppSetsJsonPath = Directory.GetCurrentDirectory() + "\\Config\\HorseRaceRe\\appsets.json";
        public static XQAPI XQAPI;

        /// <summary>
        /// 载入路径下的Buff脚本并返回BuffInfos
        /// </summary>
        /// <returns></returns>
        public static List<BuffInfo> LoadBuffs()
        {
            List<BuffInfo> list = new List<BuffInfo>();
            var dir = Directory.GetCurrentDirectory() + "\\Config\\HorseRaceRe\\Buffs";

            var dirinfo = new DirectoryInfo(dir);
            if (dirinfo.Exists is false)
                dirinfo.Create();
            foreach (var fileInfo in dirinfo.GetFiles())
            {
                try
                {
                    list.Add(new BuffInfo()
                    {
                        Buff =
                            CSScript.Evaluator.LoadCode<IBuff>(fileInfo.FullName),
                        Info = fileInfo
                    });
                }
                catch (Exception e)
                {
                }
            }

            return list;
        }

        /// <summary>
        /// 载入路径下的Event脚本并返回EventInfos
        /// </summary>
        /// <returns></returns>
        public static List<EventInfo> LoadEvents()
        {
            List<EventInfo> list = new List<EventInfo>();
            var dir = Directory.GetCurrentDirectory() + "\\Config\\HorseRaceRe\\Events";

            var dirinfo = new DirectoryInfo(dir);
            if (dirinfo.Exists is false)
                dirinfo.Create();

            foreach (var fileInfo in dirinfo.GetFiles("*.cs"))
            {
                list.Add(new EventInfo()
                    {
                        Event =
                            CSScript.Evaluator.LoadCode<IEvent>( File.ReadAllText(fileInfo.FullName)),
                        Info = fileInfo
                    });



            }

            return list;
        }
    }


    public class AppSets
    {
        public List<string> EnableGroups { get; set; } = new List<string>();

        public int MinJoin { get; set; } = 4;

        public int MaxJoin { get; set; } = 8;
        
    }
}