﻿using HorseRaceRe.Code.Models;

namespace HorseRaceRe.Code.Interfaces
{
    public interface IEvent
    {
        string Process(ref RaceGround ground, ref Horse horse, ref int randomstep);
    }
}