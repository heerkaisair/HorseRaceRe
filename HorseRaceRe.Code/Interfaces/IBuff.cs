﻿using HorseRaceRe.Code.Models;

namespace HorseRaceRe.Code.Interfaces
{
    public interface IBuff
    {
        /// <summary>
        /// Buff的一次执行
        /// </summary>
        /// <param name="ground"></param>
        /// <param name="horse"></param>
        /// <param name="randomstep"></param>
        void Process(ref RaceGround ground, ref Horse horse, ref double randomstep);
    }
}