﻿using System.Collections.Generic;
using System.Net.Http;

namespace HorseRaceRe.Code.Helpers
{
    public class HttpHelper
    {
        public static string Get(string url)
        {
            using (var client = new HttpClient())
            {
                return client.GetStringAsync(url).Result;
            }
        }

        public static string Post(string url,List<KeyValuePair<string,string>> values)
        {
            using (var client = new HttpClient())
            {

                var content = new FormUrlEncodedContent(values);
 
                var response = client.PostAsync(url, content);
 
                var responseString = response.Result.Content.ReadAsStringAsync();
                
                return responseString.Result;
            }
        }
        public static string Post(string url,string body)
        {
            using (var client = new HttpClient())
            {

                var content = new StringContent(body);
                content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.PostAsync(url, content);
 
                var responseString = response.Result.Content.ReadAsStringAsync();
                
                return responseString.Result;
            }
        }
    }
}