﻿using System.IO;

namespace HorseRaceRe.Code.Helpers
{
    public static class PathHelper
    {
        public static string NormalTrackPath =>
            Directory.GetCurrentDirectory() + "Config\\HorseRaceRe\\Resources\\跑道.jpg";

        public static string GetGroundJson(string ground) => $"Config\\HorseRaceRe\\Grounds\\{ground}.json";
        public static string GetHorseBase(string qq) => $"Config\\HorseRaceRe\\Datas\\{qq}.json";
        public static string GetUserImgPath(string qq) => $"Config\\HorseRaceRe\\Datas\\{qq}.png";
        public static string GroundsDir=> $"Config\\HorseRaceRe\\Grounds";
    }
}