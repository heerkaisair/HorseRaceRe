﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace HorseRaceRe.Code.Models
{
    /// <summary>
    /// 用于储存信息的马儿
    /// </summary>
    public class HorseBase
    {
        public static HorseBase FromFile(string path)
        {
            if (File.Exists(path) is false)
                return null;
            return JsonConvert.DeserializeObject<HorseBase>(File.ReadAllText( path));
        }

        public static void NewSave(string name,string path)
        {
            HorseBase b = new HorseBase() {Name = name};

            if (File.Exists(path))
            {
                File.WriteAllText(path,JsonConvert.SerializeObject(b));
            }
            else
            {
                File.Create(path).Close();
                File.WriteAllText(path,JsonConvert.SerializeObject(b));

            }
        }
        public string Name { get; set; }

        public List<string> Prefixs { get; set; } = new List<string>();
        
        
        
    }
}