﻿using System.IO;
using HorseRaceRe.Code.Interfaces;

namespace HorseRaceRe.Code.Models
{
    public class EventInfo
    {
        public IEvent Event;

        public FileInfo Info;
    }
}