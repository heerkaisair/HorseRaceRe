﻿using System.Collections.Generic;
using System.Linq;

namespace HorseRaceRe.Code.Models
{
    public enum HorseStatus
    {
        None,//正常状态,
        Died//已经死亡
        
    }
    /// <summary>
    /// 比赛中的马儿
    /// </summary>
    public class Horse
    {


        public Horse(HorseBase h,string qq)
        {
            
            Base = h;
            Owner = qq;

        }

        public string Owner;//需要开头就初始化的!
        
        public HorseBase Base;

        public int Position = 0;

        public Dictionary<string,int> Buffs = new Dictionary<string, int>();

        public HorseStatus Status = HorseStatus.None;

        
        /// <summary>
        /// 给赛跑中的马儿添加Buff
        /// </summary>
        /// <param name="buff"></param>
        /// <param name="times"></param>
        public void AddBuff(string buff,int times)
        {
            if (Buffs.ContainsKey(buff))
            {
                Buffs.Add(buff,times);
            }
            else
            {
                Buffs[buff] += times;
            }
        }
    }
}