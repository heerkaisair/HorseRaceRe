﻿using System.IO;
using HorseRaceRe.Code.Interfaces;

namespace HorseRaceRe.Code.Models
{
    public class BuffInfo
    {
        public IBuff Buff;

        public FileInfo Info;
    }
}