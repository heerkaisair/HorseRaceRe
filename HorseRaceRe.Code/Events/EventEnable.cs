﻿using System.Collections.Generic;
using System.IO;
using HorseRaceRe.Code.Models;
using HorseRaceRe.UI;
using Newtonsoft.Json;
using XQColorful.SDK;
using XQColorful.SDK.EventArgs;
using XQColorful.SDK.Interface;

namespace HorseRaceRe.Code.Events
{
    public class EventEnable : IPluginEvent
    {
        public XQEventType EventType => XQEventType.PluginEnable;

        public void Process(XQEventArgs e)
        {
            //检查文件夹并创建
            var s = new[] {"temp", "Buffs", "Events", "Grounds", "Resources", "Datas"};
            foreach (var s1 in s)
            {
                if (Directory.Exists("Config\\HorseRaceRe\\" + s1) is false)
                {
                    Directory.CreateDirectory("Config\\HorseRaceRe\\" + s1);
                }
            }


            //CheckJsonFile
            if (File.Exists(App.AppSetsJsonPath) is false)
            {
                File.Create(App.AppSetsJsonPath).Close();
                File.WriteAllText(App.AppSetsJsonPath, JsonConvert.SerializeObject(new AppSets()));
                //Create Sets Json
            }

            App.AppSet = JsonConvert.DeserializeObject<AppSets>( File.ReadAllText(App.AppSetsJsonPath)) ?? new AppSets();
            //Load Sets Json 

            App.LoadedEvents = App.LoadEvents();
            //Load Race Event Scripts
            
            App.LoadedBuffs = App.LoadBuffs();
            //Load Race Buff Scripts

            App.XQAPI = e.XQAPI;

            //初始化dic
            App.GroupGrounds = new Dictionary<string, RaceGround>();
        }
    }
}