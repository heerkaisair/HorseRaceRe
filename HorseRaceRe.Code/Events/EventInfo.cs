﻿using System.Text;
using XQColorful.SDK;
using XQColorful.SDK.EventArgs;
using XQColorful.SDK.Interface;

namespace HorseRaceRe.Code.Events
{
    public class EventInfo : IPluginEvent
    {
        public XQEventType EventType => XQEventType.Group;


        public void Process(XQEventArgs _e)
        {
            var e = new GroupMsgEventArgs(_e);
            if (e.Message.ToLower() == "赛马re" || e.Message.ToLower()=="赛马re info")
            {
                var msg = new StringBuilder();

                msg.AppendLine($"欢迎使用赛马Re - by Heer");
                msg.AppendLine($"当前版本: {App.Core.Version}");
                msg.AppendLine($"当前载入事件数量: {App.LoadedEvents.Count}");
                if (App.Core.CheckUpdate(out var version))
                {
                    msg.AppendLine($"最新版本: {version.Version}!");
                    msg.AppendLine($"更新日志: {version.UpdateLog}!");
                    msg.AppendLine($"Bot主人可以在{version.DownloadLink}获取更新!");
                    msg.AppendLine($"或者加入赛马Re/赫尔的后援会 得到更新!");

                }
                else
                {
                    msg.AppendLine($"目前已经是最新版啦!");
                }
                e.SendMsg(msg.ToString());
            }
        }
    }
}