﻿using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using HorseRaceRe.Code.Helpers;
using HorseRaceRe.Code.Models;
using XQColorful.SDK;
using XQColorful.SDK.EventArgs;
using XQColorful.SDK.Interface;
using XQColorful.SDK.Models;

namespace HorseRaceRe.Code.Events
{
    public class EventCreateHorse: IPluginEvent
    {
        public XQEventType EventType => XQEventType.Group;

        public void Process(XQEventArgs _e)
        {
            var e = new GroupMsgEventArgs(_e);

            if (e.Message == "创建马儿")
            {
                var msg = new StringBuilder();
                msg.AppendLine("发送 创建马儿 Emoji 即可为你的re马儿创建身份信息!");
                msg.AppendLine("如果安装了赛马2联动扩展可以使用赛马2的信息!(暂未启用)");
                e.SendMsg(msg.ToString());
            }

            if (e.Message.StartsWith("创建马儿"))
            {
                var infos = e.Message.TrimStart("创建马儿".ToCharArray()).Trim();
                var name = infos.Trim();

                HorseBase.NewSave(name,PathHelper.GetHorseBase(e.FromQQ));
                
                e.SendMsg(XQCode.At(e.FromQQ) + "创建完成!");


            }
        }
    }
}